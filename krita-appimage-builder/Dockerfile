FROM ubuntu:20.04

LABEL Description="KDE Appimage Base"
MAINTAINER KDE Sysadmin <sysadmin@kde.org>

# Start off as root
USER root

# Setup the various repositories we are going to need for our dependencies
RUN apt-get update && apt-get install -y apt-transport-https ca-certificates gnupg software-properties-common wget
RUN wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | apt-key add -
RUN add-apt-repository -y ppa:openjdk-r/ppa && apt-add-repository 'deb https://apt.kitware.com/ubuntu/ focal main'

# Update the system...
RUN apt-get update && apt-get upgrade -y

# Some software demands a newer GCC because they're using C++14 stuff, which is just insane
# We do this after the general system update to ensure it doesn't bring in any unnecessary updates
RUN add-apt-repository -y ppa:ubuntu-toolchain-r/test && apt-get update

# Krita's dependencies (libheif's avif plugins) need Rust 
RUN add-apt-repository -y ppa:ubuntu-mozilla-security/rust-updates && apt-get update && apt-get install -y cargo rustc

ENV TZ=UTC
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Now install the general dependencies we need for builds
RUN apt-get install -y \
  # General requirements for building KDE software
  build-essential gcc-11 g++-11 cmake git-core locales rsync \
  # General requirements for building other software
  automake libxml-parser-perl libpq-dev libaio-dev \
  # Needed for some frameworks
  bison gettext \
  # Base system deps
  gperf libnss3-dev libpci-dev libatkmm-1.6-dev libbz2-dev libcap-dev libdbus-1-dev libudev-dev \
  # OpenSSL (1.1.1f) and libgcrypt (1.8.5) libraries 
  libssl-dev libgcrypt-dev \
  # DRM and openGL libraries
  libdrm-dev libegl1-mesa-dev libgl1-mesa-dev mesa-common-dev \
  # Font libraries (TODO: consider removal)
  libfontconfig1-dev libfreetype6-dev \
  # GNU Scientific Library
  libgsl-dev \
  # GStreamer pugins for Qt Multimedia
  libpulse-dev libasound2-dev \
  gstreamer1.0-alsa gstreamer1.0-pulseaudio gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-ugly \
  libgstreamer1.0-dev libgstreamer-plugins-good1.0-dev libgstreamer-plugins-bad1.0-dev libgstreamer-plugins-base1.0-dev \
  # XCB Libraries for Qt
  libwayland-dev \
  libicu-dev libxcb-shm0-dev libxinerama-dev libxcb-icccm4-dev libxcb-xinerama0-dev libxcb-image0-dev libxcb-render-util0-dev \
  libx11-dev libxkbcommon-x11-dev libxcb-glx0-dev libxcb-keysyms1-dev libxcb-util0-dev libxcb-res0-dev libxcb1-dev \
  libxcomposite-dev libxcursor-dev libxdamage-dev libxext-dev libxfixes-dev libxi-dev libxrandr-dev libxrender-dev \
  libxcb-randr0-dev libxcb-shape0-dev libxcb-xfixes0-dev libxcb-sync-dev libxcb-xinput-dev \
  libxss-dev libxtst-dev \
  # Krita AppImage Python extra dependencies
  libffi-dev \
  # Other
  flex \
  # cppcheck is necessary for the CI
  cppcheck

# Since recently we build Python ourselves, so there is no need for the deadsnake's repo
# (at least until we recover system-provided python)
## The deadsnakes PPA packs setuptools and pip inside python3.10-venv
# RUN add-apt-repository -y ppa:deadsnakes/ppa && apt-get update && apt-get install -y python3.10 python3.10-dev python3.10-venv && python3.10 -m ensurepip 

# Krita's dependencies (libheif's avif plugins) need meson and ninja, both aren't available in binary form for 20.04
RUN apt-get install -y python3-pip && python3 -m pip install meson ninja

RUN apt-get install --yes ccache python3-yaml python3-packaging python3-lxml python3-clint openbox xvfb dbus-x11
# See bug for gcovr: https://github.com/gcovr/gcovr/issues/583
RUN python3 -m pip install python-gitlab gcovr==5.0 cppcheck-codequality

RUN update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-11 20 && \
     update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 10 && \
     update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-11 20 && \
     update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-9 10 && \
     update-alternatives --install /usr/bin/gcov gcov /usr/bin/gcov-11 20 && \
     update-alternatives --install /usr/bin/gcov gcov /usr/bin/gcov-9 10 && \
     update-alternatives --install /usr/bin/gcov-dump gcov-dump /usr/bin/gcov-dump-11 20 && \
     update-alternatives --install /usr/bin/gcov-dump gcov-dump /usr/bin/gcov-dump-9 10 && \
     update-alternatives --install /usr/bin/gcov-tool gcov-tool /usr/bin/gcov-tool-11 20 && \
     update-alternatives --install /usr/bin/gcov-tool gcov-tool /usr/bin/gcov-tool-9 10

# For D-Bus to be willing to start it needs a Machine ID
RUN dbus-uuidgen > /etc/machine-id
# Certain X11 based software is very particular about permissions and ownership around /tmp/.X11-unix/ so ensure this is right
RUN mkdir /tmp/.X11-unix/ && chown root:root /tmp/.X11-unix/ && chmod 1777 /tmp/.X11-unix/

# Setup a user account for everything else to be done under
RUN useradd -d /home/appimage/ -u 1000 --user-group --create-home -G video appimage
# Make sure SSHD will be able to startup
RUN mkdir /var/run/sshd/
# Get locales in order
RUN locale-gen en_US en_US.UTF-8 en_NZ.UTF-8

# Switch over to our new user and add in the utilities needed for appimage builds
USER appimage
COPY setup-utilities /home/appimage/
RUN /home/appimage/setup-utilities
